/* 07/12/2018 CW wrote for compariason/discussion/ best practice
 * 
 * 
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


//csv file
/*
GUID,Val1,Val2,Val3
g0,1,1,1
g1,2,2,2
g2,3,3,3
g2,4,4,4
g3,5,55,5
g4,6,6,6
g4,7,7,7
g5,8,8,8
*/
namespace BryanTest
{

    /// <summary>
    /// Keep in mind with large flat files there is memory considerations, 
    /// this app is ran best in 64b machine with appropriate app.config/ project settings applied E.G 
    ///   <runtime>
    ///  <gcAllowVeryLargeObjects enabled = "true" />
    /// </ runtime >
    /// </summary>
    class Program
    {
        /// <summary>
        /// console app entry point expects no parameters
        /// </summary>
        static void Main()
        {


            List<InputFile> recs = InputFileMassage();
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            OutputFileMassage(recs);

        }


        /// <summary>
        /// opens up a csv file, loads into a list, and massages accordingly
        /// </summary>
        /// <returns></returns>
        private static List<InputFile> InputFileMassage()
        {

            //            Build a C# console application to parse this file and output a result .csv file.
            //C# Console Output:
            //•             Output the total number of records in the file.
            //•             Show the largest sum of Val1 and Val2 for any single row in the CSV, as well as the GUID for that row.
            //•             Show any Duplicate GUID values.
            //•             Show the average length of Val3 across all input rows.

            List<InputFile> inputFiles = new List<InputFile>();
            // no need to flush
            var csvFile = File.ReadLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "inputFile.csv"));
            bool firstLine = true;

            foreach (var line in csvFile)
            {
                string[] parsed = new string[0];

                if (!firstLine)// dont want to include header row
                {
                    parsed = line.Split(',');
                    if (parsed.Length == 4)
                    {
                        InputFile input = new InputFile
                        {
                            Guid = parsed[0],
                            Val1 = int.Parse(parsed[1]),
                            Val2 = int.Parse(parsed[2]),
                            Val3 = int.Parse(parsed[3]),
                        };

                        input.AddV = input.Val1 + input.Val2;
                        inputFiles.Add(input);
                    }  //?  else   {  } //bad data out of scope?
                }
                firstLine = false;
            }

            double avgV3 = AvgValThree(inputFiles);

            List<string> dupeList = inputFiles.GroupBy(i => i.Guid)
                .Where(g => g.Count() >= 2)
                .Select(g => new { Code = g.Key, Desc = g.First().Guid })
                .ToDictionary(x => x.Code, x => x.Desc)
                .Select(x => x.Value).ToList();

            string dupes = string.Join(",", dupeList);

            //iterating thru dupes and marking them
            dupeList.ForEach(f =>
            {
                inputFiles.FindAll(obj => f.ToString() == obj.Guid).ForEach(inF =>
                {
                    inF.IsDupe = true;
                });
            });

            //grabbing largest sum row
            InputFile largest = inputFiles.OrderByDescending(o => o.AddV).First();

            Console.WriteLine(string.Format("{0} , {1}, {2}, {3}",
                "count :" + inputFiles.Count
                , "largest sum Guid/Value :" + largest.Guid + " / " + largest.AddV
                , "dupes: " + dupes
                , "average length of Val3 :" + avgV3));


            return inputFiles;
        }



        /// <summary>
        /// takes in a list of records and creates a csv named output.csv file located in the calling executable dir
        /// </summary>
        /// <param name="inputFiles"></param>
        private static void OutputFileMassage(List<InputFile> inputFiles)
        {        //Output CSV file:
                 //•             For each input row:
                 //•             Output columns:
                 //•             GUID
                 //•             Val1 + Val2
                 //•             IsDuplicateGuid(Y or N)
                 //•             Is Val3 length greater than the average length of Val3(Y or N)
                 //Please send back a link to a publicly hosted repository containing your code.


            StringBuilder stringBuilder = new StringBuilder();
            double avgV3 = AvgValThree(inputFiles);
            foreach (InputFile line in inputFiles)
            {
                stringBuilder.AppendLine(string.Format("{0} , {1}, {2}, {3}", line.Guid, line.AddV, line.IsDupe, line.Val3 > avgV3));
            }
            // no need to flush/ file is overwritten with each app execution
            //header row not specified in requirements
            File.WriteAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "outputFile.csv"), stringBuilder.ToString());

        }

        /// <summary>
        /// maybe overkill making a separate method , but it is reused....
        /// </summary>
        /// <param name="inputFiles"></param>
        /// <returns></returns>
        private static double AvgValThree(List<InputFile> inputFiles)
        {
            return inputFiles.Average(item => item.Val3);
        }
    }


    /// <summary>
    /// Class used to track csv records and modification
    /// </summary>
    class InputFile
    {
        public string Guid { get; set; }
        public int Val1 { get; set; }
        public int Val2 { get; set; }
        public int Val3 { get; set; }
        public bool IsDupe { get; set; }
        public double AddV { get; set; }
    }

}
